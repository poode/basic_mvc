<footer id="footer">
        <div class="footer-bottom">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <p>&copy; جميع الحوق محفوظة.</p>
              </div>
              <div class="col-sm-6">
                <p class="pull-right">تصميم وبرمجة <a href="https://www.facebook.com/webmisr">عبد المنعم فتحي</a></p>
              </div>
            </div>
          </div>
        </div>
      </footer>