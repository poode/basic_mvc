<!DOCTYPE html>
<html lang="en">
  <head>
    <title>  اتصل بنا شركة التحرير للاستثمار الزراعي والتنمية العمرانية</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="dns-prefetch" href="http://ajax.googleapis.com/">
    <link rel="dns-prefetch" href="http://s.w.org/">
    <script src="./views/contact_us_files/headlogic.js" defer=""></script>
    <script src="./views/contact_us_files/wp-emoji-release.min.js.download" type="text/javascript" defer=""></script>
    <link rel="stylesheet" id="customstyle-css" href="./views/contact_us_files/customstyle.css" type="text/css" media="all">
    <link rel="stylesheet" id="bootstrab-css-css" href="./views/contact_us_files/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="font-awesome-css-css" href="./views/contact_us_files/font-awesome.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="animate-css-css" href="./views/contact_us_files/animate.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="light-css-css" href="./views/contact_us_files/lightbox.css" type="text/css" media="all">
    <link rel="stylesheet" id="main-css-css" href="./views/contact_us_files/main.css" type="text/css" media="all">
    <link rel="stylesheet" id="preset-css-css" href="./views/contact_us_files/preset1.css" type="text/css" media="all">
    <link rel="stylesheet" id="responsive-css-css" href="./views/contact_us_files/responsive.css" type="text/css" media="all">
    <link rel="stylesheet" id="slb_core-css" href="./views/contact_us_files/app.css" type="text/css" media="all">
    <link rel="https://api.w.org/" href="http://www.al-tahrir.net/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.al-tahrir.net/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.al-tahrir.net/wp-includes/wlwmanifest.xml">
    <meta name="generator" content="WordPress 4.8.5">
    <link rel="canonical" href="http://www.al-tahrir.net/%d8%a7%d8%aa%d8%b5%d9%84-%d8%a8%d9%86%d8%a7/">
    <link rel="shortlink" href="http://www.al-tahrir.net/?p=551">
    <link rel="alternate" type="application/json+oembed" href="http://www.al-tahrir.net/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.al-tahrir.net%2F%25d8%25a7%25d8%25aa%25d8%25b5%25d9%2584-%25d8%25a8%25d9%2586%25d8%25a7%2F">
    <link rel="alternate" type="text/xml+oembed" href="http://www.al-tahrir.net/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.al-tahrir.net%2F%25d8%25a7%25d8%25aa%25d8%25b5%25d9%2584-%25d8%25a8%25d9%2586%25d8%25a7%2F&amp;format=xml">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <?php include('./views/nav.php'); ?>

    <?php include('./views/content.php'); ?>

    <?php include('./views/footer.php') ?>

      <script type='text/javascript' src='./views/contact_us_files/jquery-3.3.1.min.js'></script>
      <script type='text/javascript' src='http://www.al-tahrir.net/wp-content/themes/golden-theme/js/bootstrap.min.js?ver=4.8.5'></script>
      <script type='text/javascript' src='http://www.al-tahrir.net/wp-content/themes/golden-theme/js/jquery.inview.min.js?ver=4.8.5'></script>
      <script type='text/javascript' src='http://www.al-tahrir.net/wp-content/themes/golden-theme/js/wow.min.js?ver=4.8.5'></script>
      <script type='text/javascript' src='http://www.al-tahrir.net/wp-content/themes/golden-theme/js/mousescroll.js?ver=4.8.5'></script>
      <script type='text/javascript' src='http://www.al-tahrir.net/wp-content/themes/golden-theme/js/smoothscroll.js?ver=4.8.5'></script>
      <script type='text/javascript' src='http://www.al-tahrir.net/wp-content/themes/golden-theme/js/jquery.countTo.js?ver=4.8.5'></script>
      <script type='text/javascript' src='http://www.al-tahrir.net/wp-content/themes/golden-theme/js/lightbox.min.js?ver=4.8.5'></script>
      <script type='text/javascript' src='http://www.al-tahrir.net/wp-content/themes/golden-theme/js/main.js?ver=4.8.5'></script>
      <script type='text/javascript' src='http://www.al-tahrir.net/wp-includes/js/wp-embed.min.js?ver=4.8.5'></script>
      <script type="text/javascript" id="slb_context">/* <![CDATA[ */if ( !!window.jQuery ) {(function($){$(document).ready(function(){if ( !!window.SLB ) { {$.extend(SLB, {"context":["public","user_guest"]});} }})})(jQuery);}/* ]]> */
      </script>
      
  </body>
</html>