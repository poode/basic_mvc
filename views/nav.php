<div class="main-nav">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">
        <h1><img class="img-responsive" src="http://www.al-tahrir.net/wp-content/themes/golden-theme/images/logo.png" alt="logo" style="height: 87px;margin-top: -22px;"></h1>
      </a>
    </div>
    
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li class="scroll"><a href="/">الرئيسية</a></li>
        <li class="scroll"><a href="http://www.al-tahrir.net/%d9%85%d9%86-%d9%86%d8%ad%d9%86/">من نحن</a></li>
        <li class="scroll"><a href="/category/project/">مشاريعنا</a></li>
        <li class="scroll"><a href="/category/services/">خدماتنا</a></li>
        <li class="scroll"><a href="/category/gallery/">معرض الصور</a></li>
        <li class="scroll"><a href="/category/videos/">الفيديو</a></li>
        <li class="scroll active"><a href="http://www.al-tahrir.net/%d8%a7%d8%aa%d8%b5%d9%84-%d8%a8%d9%86%d8%a7/">اتصل بنا</a></li>
      </ul>
    </div>
  </div>
  </div><!--/#main-nav-->