    <section id="inner" class="parallax">
      <div class="container">
        <div class="row">
          <div class="col-sm-2">
          </div>
          <div class="col-sm-8">
            <div class="about-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
              <h1>اتصل بنا</h1>
              <p style="text-align: center;">مكتب القاهرة: 12 عمارات صقر قريش &#8211; مساكن شيراتون &#8211; مصر الجديدة</p>
              <p style="text-align: center;">مكتب الخرطوم: شارع الستين &#8211; تقاطع شارع المشتل &#8211; بجوار صيدلية العريفى</p>
              <p style="text-align: center;">(+2)01111666648 &#8211; (+2)01093307789 &#8211; (+966)556700853  &#8211; (+2)0222695525</p>
              <p style="text-align: center;">Website: www.al-tahrir.com</p>
              <p style="text-align: center;">Email: sales@al-tahrir.com</p>
              
            </div>
            <div class="col-sm-2">
            </div>
          </div>
        </div>
      </section>
    </div>
    <section id="contact">
      <div id="google-map" class="wow fadeIn" data-wow-duration="1000ms" data-wow-delay="400ms">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3451.51037529567!2d31.36826131539757!3d30.108205981859076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzDCsDA2JzI5LjUiTiAzMcKwMjInMTMuNiJF!5e0!3m2!1sen!2sus!4v1480257063993" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
      </div>
      <div id="contact-us" class="parallax">
        <div class="container">
          <div class="row">
            <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
              <h2>تواصل معنا الان</h2>
              <p>يمكنكم التواصل معنا سعياً منا لتحقيق المزيد من التواصل و تلقى استفسارتكم واقتراحتكم التى تتعلق بمشاريعنا</p>
            </div>
          </div>
          <div class="contact-form wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
              <div class="col-sm-6">
                <form id="main-contact-form" name="contact-form" method="post" action="#">
                  <div class="row  wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="الايميل" required="required">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="الاسم" required="required">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="text" name="subject" class="form-control" placeholder="العنوان" required="required">
                  </div>
                  <div class="form-group">
                    <textarea name="message" id="message" class="form-control" rows="4" placeholder="الرسالة" required="required"></textarea>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn-submit">ارسل الان</button>
                  </div>
                </form>
              </div>
              <div class="col-sm-6">
                <div class="contact-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <ul class="address">
                    <li><i class="fa fa-map-marker"></i> <span> العنوان:</span> 12 عمارات صقر قريش - مساكن شيراتون - مصر الجديدة  </li>
                    <li><i class="fa fa-phone"></i> <span> الجوال:</span> <a href="tel:01111666648">01111666648</a> / <a href="tel:01093307789">01093307789</a> / <a href="tel:0222695525">0222695525 </a> / <a href="tel:00966556700853">00966556700853 </a> </li>
                    <li><i class="fa fa-envelope"></i> <span> الايميل:</span><a href="mailto:sales@al-tahrir.com"> sales@al-tahrir.com</a></li>
                    <li><i class="fa fa-globe"></i> <span> الموقع الالكتروني:</span> <a href="http://www.al-tahrir.com">http://www.al-tahrir.com</a></li>
                  </ul>
                  <div class="social text-center">
                    <a href="http://www.facebook.com"><i class="fa fa-facebook fa-3x fac"></i></a>
                    <a href="http://www.twitter.com"><i class="fa fa-twitter fa-3x twi"></i></a>
                    <a href="http://www.google.com"><i class="fa fa-google-plus fa-3x goo"></i></a>
                    <a href="http://www.youtube.com"><i class="fa fa-youtube fa-3x you"></i></a>
                    <a href="http://www.pinterest.com"><i class="fa fa-pinterest-p fa-3x pin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </section><!--/#contact-->