<?php 


function request_path()
{
    $request_uri = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
    $script_name = explode('/', trim($_SERVER['SCRIPT_NAME'], '/'));
    $parts = array_diff_assoc($request_uri, $script_name);
    if (empty($parts))
    {
        return '/';
    }
    $path = implode('/', $parts);
    if (($position = strpos($path, '?')) !== FALSE)
    {
        $path = substr($path, 0, $position);
    }
    return $path;
}


function loadControllerAndMethod($controllerName,$method) {
  if (file_exists('./controller/'.$controllerName.'.php')) {
    require_once ('./controller/'.$controllerName.'.php');
    $methodsArray = get_class_methods($controllerName);
    foreach ($methodsArray as $methodInArray) {
     if ($methodInArray === $method) {
       $controller = new $controllerName;
       $controller->index();
       return false;
     }
    }
  }
}

