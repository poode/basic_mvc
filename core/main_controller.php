<?php


/**
* the main controller that will handle rendering of the view
*/
class Controller 
{
	
	function __construct()
	{
		
	}

	static function view($view_folder,$view_file)   // $view_folder like 'error'
	{
		$view_name_file = 'views/'.$view_folder.'/'.$view_file.'.php';
  		if (file_exists($view_name_file)) {
  			require_once $view_name_file;
  		}
  		else {
  			echo 'view not exists';
  		}
	}	
}